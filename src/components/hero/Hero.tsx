import styled from "styled-components";
import HeroText from "./HeroText";

const HeroContainer = styled.div`
  height: 20em;
  width: 100%;
  display: flex;
  flex-direction: row;
`;

export default function Hero() {
  return (
    <HeroContainer>
      <div></div>
      <HeroText />
    </HeroContainer>
  );
}
