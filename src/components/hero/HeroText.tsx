import styled from "styled-components";

const HeroTextEl = styled.div`
  align-self: end;
  height: 100%;
`;

export default function HeroText() {
  return (
    <HeroTextEl>
      <h3>Hi, I'm Curtis</h3>
    </HeroTextEl>
  );
}
